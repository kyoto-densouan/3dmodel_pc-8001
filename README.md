# README #

1/3スケールのNEC PC-8001風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1979年5月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-8001)
- [IPSJコンピュータ博物館](https://museum.ipsj.or.jp/computer/personal/0001.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001/raw/47d586514d213ddcf5fb302d5e919d7d0fb695f3/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001/raw/47d586514d213ddcf5fb302d5e919d7d0fb695f3/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001/raw/47d586514d213ddcf5fb302d5e919d7d0fb695f3/ExampleImage.png)
